/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author Lara10
 */
public class Losango extends Figura {
    
    private double lado;
    private double diagonalPrincipal;
    private double diagonalSecundaria;
    
    public Losango (double lado, double diagonalPrincipal, double diagonalSecundaria){
        super(diagonalPrincipal*diagonalSecundaria/2, lado*4);
         
    }

    public double getLado() {
        return lado;
    }

    public void setLado(double lado) {
        this.lado = lado;
    }

    public double getDiagonalPrincipal() {
        return diagonalPrincipal;
    }

    public void setDiagonalPrincipal(double diagonalPrincipal) {
        this.diagonalPrincipal = diagonalPrincipal;
    }

    public double getDiagonalSecundaria() {
        return diagonalSecundaria;
    }

    public void setDiagonalSecundaria(double diagonalSecundaria) {
        this.diagonalSecundaria = diagonalSecundaria;
    }
    
    
}
