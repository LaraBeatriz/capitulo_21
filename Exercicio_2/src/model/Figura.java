/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author Lara10
 */
public class Figura {
    
    //Atributos
    private double area;
    private double perimetro;
    
    //Construtor
    public Figura (double area, double perimetro){
        this.area = area;
        this.perimetro = perimetro;
    }
    
    //Metodos:
    public double getArea() {
        return area;
    }

    public double getPerimetro() {
        return perimetro;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public void setPerimetro(double perimetro) {
        this.perimetro = perimetro;
    }
    
}
