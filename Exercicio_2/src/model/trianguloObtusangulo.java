/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author Lara10
 */
public class TrianguloObtusangulo extends Triangulo {
    
    private int maiorAngulo;
    
    public TrianguloObtusangulo(int maiorAngulo, double base, double altura, double perimetro, double area){
        super(perimetro,area, altura, base);
        this.maiorAngulo = maiorAngulo;
    }
    
}
