/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;
/**
 *
 * @author Lara10
 */
public class TrianguloRetangulo extends Triangulo{
    private double hipotenusa;
    
    public TrianguloRetangulo(double base, double altura){
        super(base*altura,base+altura+Math.sqrt(base*base+altura*altura),altura,base);
    
    }

    public double getHipotenusa() {
        return hipotenusa;
    }

    public void setHipotenusa(double hipotenusa) {
        this.hipotenusa = hipotenusa;
    }
   
}
