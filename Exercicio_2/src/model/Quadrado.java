/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author Lara10
 */
public class Quadrado extends Figura {
    
    private double lado;

    
    public Quadrado (double lado){
        super((lado*lado),(lado*4));

    }
    
    public Double getLado(){
        return lado;
    }
  
    public void setLado(Double lado){
        this.lado = lado;
    }
   
    
    
}
