/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package heranca.reino;

import heranca.SerVivo;

/**
 *
 * @author Lara10
 */
public class Animal extends SerVivo {
    String colunaVertebral;
    String alimento;
    

    public Animal(String colunaVertebral, String alimento) {
        super ("heterotrofos","multicelulares", "sexuada");
        this.colunaVertebral = colunaVertebral;
        this.alimento = alimento;
    }
    
    public void locomover (){}
    public void alimentar (){}
    
}
